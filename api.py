from __future__ import print_function
import datetime
from googleapiclient.discovery import build
from httplib2 import Http
from oauth2client import file, client, tools

import pandas as pd

def build_service():
    SCOPES = 'https://www.googleapis.com/auth/calendar'
    store = file.Storage('token.json')
    creds = store.get()
    if not creds or creds.invalid:
        flow = client.flow_from_clientsecrets(
            'credentials.json',
            SCOPES
        )
        creds = tools.run_flow(flow, store)
    service = build('calendar', 'v3', http=creds.authorize(Http()))
    return service

def delete_calendar(service, calendar_name):
    # 1st API call - get id of calendar to be deleted
    req_results = service.calendarList().list().execute()
    calendars = req_results.get('items')
    cal_details = [cal
        for cal in calendars
        if cal['summary'] == calendar_name]
    if not cal_details:
        print(f'WARN: The calendar "{calendar_name}" cannot be deleted '
            'because it does not exist')
        return
    cal_id = cal_details[0]['id']

    # 2nd API call - delete calendar
    service.calendars().delete(calendarId=cal_id).execute()

def insert_calendar(service, calendar_name):
    calendar = {
        'summary': calendar_name,
        'timeZone': 'America/New_York'
    }
    created_calendar = service.calendars().insert(
        body=calendar
    ).execute()
    return created_calendar['id']

def insert_event(service, cal_id, event):
    created_event = service.events().insert(
        calendarId = cal_id,
        body = event
    ).execute()

def prepare_events():
    
    df = pd.read_pickle('./calendar_df.pickle')

    # Create a column with abbreviated event names
    def trim(s):
        if len(s) > 20:
            return f'{s[:20]}...'
        else:
            return s
    df['abbrev'] = df.text.apply(trim)

    # Cast index as a string
    df.index = df.index.astype(str)

    # Group data into a list of dicts
    events_dict = df[['text', 'abbrev']].to_dict(orient='index')
    events_list = []
    for key in events_dict.keys():
        events_list.append({
            'summary': events_dict[key]['abbrev'],
            'description': events_dict[key]['text'],
            'start': {'date': key},
            'end': {'date': key}
        })
    return events_list
    
def main():
    service = build_service()

    # Delete and recreate calendar
    calendar_name = 'Academic'
    delete_calendar(service, calendar_name)
    cal_id = insert_calendar(service, calendar_name)

    # Populate calendar with events
    events = prepare_events()
    for e in events:
        insert_event(service, cal_id, e)

if __name__ == '__main__':
    main()
