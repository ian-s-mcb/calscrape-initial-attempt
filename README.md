# calscrape
DSE-I1020 - HW-3 - Problem 2 - Scrape the CCNY calendar

## Problem description
Create a private repo on github called calscrape. Share it with mdogy.
Create an ipython notebook and commit it. I want you to keep committing
it as you edit it so I can see it build up. Your mission is to scrape
the cuny fall 2018 academic calendar site using the requests library,
beautiful soup and pandas, just like in class. You should wind up with a
pandas data frame where the index column is a python date. There should
be a column for "day of the week" with variable lable dow and a column
called text with the explanation. 

Print out this data frame. 5 extra credit points if you figure out how
to use the google api to programatically add these dates to your google
calendar with the request library.

## How to run this code
```bash
# Make a virtual env for the Python dependencies
# If you have virtualenvwrapper installed, run:
mkvirtualenv --python=python3 calscrape

# Install Python dependencies
pip install -r ./requirements.txt

# Open Jupyer notebook
jupyter notebook
# Run all cells in calscrape.ipynb
# This will output the pickled DataFrame file needed for the next steps

# Enable API access to your Google account by visiting:
# https://developers.google.com/calendar/quickstart/python

# Move the downloaded credentials.json from the previous step to the
# current directory

# Run the api script
# It should open up a tab in your browser to authorize the script
# Script takes ~30 sec to run
# Script adds the events to a new calendar named 'Academic'
python api.py
```

## Screenshots of results
* ![screenshot-1-dataframe](/screenshot-1-dataframe.png?raw=true)
* ![screenshot-2-google_calendar](/screenshot-2-google_calendar.png?raw=true)
